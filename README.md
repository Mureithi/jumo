Authored by *Mureithi* [<kelvinmwas@gmail.com>]

# Jumo Engineering Tech Assessment

This app a program that will send the amount of airtime to each
employee as required by Company XYZ LTD
It is written in **Laravel** and **Angular**.

## Brief
Laravel Angular startup. These include:
  + Laravel 5.4 [Backend] (https://laravel.com/docs/5.4)
    + Passport (https://laravel.com/docs/5.4/passport)
    + Dingo (https://github.com/dingo/api)

  + AngularJS (1.6.\*) [Frontend]
    + Bower (Package Manager) (https://bower.io/)
    + Gulp (http://gulpjs.com/)

## INSTALLATION
For installation instructions, straight from GIT, read [this](INSTALL.md).

## ASSUMPTIONS
Assumptions made in development read [this](ASSUMPTIONS.md).
