angular.module('module.home', []).controller('homeCtrl', [
    '$scope',
    'Requests',
    '$state',
    '$parse',
    function(scope, Requests, state, parse,) {

      scope.test = [];
      scope.csvObj = [];
      scope.csv = {
    	content: null,
    	header: true,
    	headerVisible: true,
    	separator: ',',
    	separatorVisible: true,
    	result: null,
    	encoding: 'ISO-8859-1',
    	encodingVisible: true,
        progressCallback: function(progress) {
            scope.$apply( function() {
                scope.progress = progress;
            });
        },
        streamingCallback: function(stream) {
           if ( typeof stream != "undefined" ) {
               scope.$apply( function() {
                   scope.preview = stream[Math.floor(Math.random()*stream.length)];
               });
           }
       }
      }


      var _lastGoodResult = '';

      scope.reformObj = function (json, tabWidth) {

      		var objStr = JSON.stringify(json);
      		var obj = null;
      		try {
      			obj = parse(objStr)({});
      		} catch(e){
      			// eat $parse error
      			return _lastGoodResult;
      		}
          console.log(obj);return;

      		var result = JSON.stringify(obj, null, Number(tabWidth));
      		_lastGoodResult = result;

          // console.log(result);

      		return result;

      }

      scope.send = function send() {
          var payload = scope.csv.result;
          // console.log(payload);

          Requests.post('send', payload, function(data) {
              console.log(data);
              // if(data.message.success){
              //     state.go('home.all')
              // }

          });
      }


  }
])
