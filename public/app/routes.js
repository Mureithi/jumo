/**
 * @ngdoc config
 * @name mainRouteConfig
 * @memberof ClientApp
 * @param $stateProvider {service}
 * @param $urlRouterProvider {service}
 */
 angular.module("jumo").config(function($stateProvider, $urlRouterProvider) {
   $urlRouterProvider.otherwise("/home/all");
 });
