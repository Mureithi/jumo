<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;
use App\User;

class UsersController extends Controller
{
  public function index()
  {
      $user = User::all();

      if($user){
          return Response::json($user);
      }
  }
}
