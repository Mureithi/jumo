## 1. Fork the project
The app can be found [here](https://gitlab.com/Mureithi/jumo)

## 2. Clone the project
Copy the source code to your *WORKING DIRECTORY*

## 3. Dependencies
Update your composer dependencies by running the following command:

```
composer install
```
Update your node dependencies by running the following command:

```
npm install
```

Update your bower dependencies by running the following command:

```
bower install
```

## 4. Running the App
<!--
An important step is connecting your instance of the application to MySQl.
This will involve editing the `database.php` file found here (starred):

```
.
├── .DS_Store
├──*.env
├── .env.example
…
```

and changing these values:

```
DB_DATABASE= YOURDBNAME
DB_USERNAME= YOURDBUSER
DB_PASSWORD= YOURDBPASSWORD
```

***
If this file (*.env) does not appear, create it. -->

<!-- ## 5. MySQL
Ensure that MySQL is installed in your machine.

create an empty schema with a name of your choice.

In your root folder of the app run the migrations command

```
php artisan migrate --seed
``` -->
In the WORKING DIRECTORY run:

php artisan serve

The app should be up and running on (http://127.0.0.1:8000)
