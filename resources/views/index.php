<html ng-app='jumo'>

<!-- Head -->
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<link rel="stylesheet" href="css/styles.css">
    <link rel='stylesheet' href='libs/css/slick.css'>
    <link rel='stylesheet' href='libs/css/bootstrap.css'>
	<link rel="stylesheet" href="libs/css/textAngular.css">
	<link rel="stylesheet" href="libs/css/font-awesome.css">
	<link rel="stylesheet" href="libs/css/ionicons.css">
	<link rel="stylesheet" href="libs/css/loading-bar.css">
    <link rel='stylesheet' href='libs/css/textAngular.css'>
    <link rel='stylesheet' href='libs/css/slick.css'>
    <link rel='stylesheet' href='libs/css/angular-toastr.css'>

    <link rel=stylesheet href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<link href='https://fonts.googleapis.com/css?family=Lato:400,300,700' rel=stylesheet type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel=stylesheet type='text/css'>
	<script src="libs/js/jquery.js"></script>
	<script src="libs/js/angular.js"></script>
    <script src="libs/js/lodash.compat.js"></script>
	<title ng-bind="title"></title>

	<!-- <link rel="stylesheet" href="dist/styles.css"> -->
</head>
<body>

<section ui-view>

</section>


<!-- Footer -->

<footer class="footer" style="bottom:inherit;">
    <div class="container">
        <span class="text-muted">Copyright © 2018.</span>
    </div>
</footer>


<script src="libs/js/angular-ui-router.js"></script>
<script src="libs/js/restangular.js"></script>
<script src="libs/js/ng-file-upload.js"></script>
<script src='libs/js/angular-local-storage.js'></script>
<script src="libs/js/ui-bootstrap-tpls.js"></script>
<script src="libs/js/loading-bar.js"></script>
<script src="libs/js/highcharts.js"></script>
<script src="libs/js/highcharts-ng.js"></script>
<script src="libs/js/moment.js"></script>
<script src="libs/js/angular-moment.js"></script>
<script src="libs/js/ui-bootstrap-tpls.js"></script>
<script src="libs/js/bootstrap.js"></script>
<script src="libs/js/smart-table.min.js"></script>
<script src="libs/js/angular-filter.js"></script>
<script src="libs/js/angular-validator.js"></script>
<script src="libs/js/rangy-core.js"></script>
<script src="libs/js/rangy-selectionsaverestore.js"></script>
<script src="libs/js/textAngularSetup.js"></script>
<script src="libs/js/textAngular-sanitize.js"></script>
<script src="libs/js/textAngular.js"></script>
<script src="libs/js/angular-animate.js"></script>
<script src="libs/js/angular-toastr.tpls.js"></script>
<script src="libs/js/angular-csv-import.js"></script>
<script src="https://pc035860.github.io/angular-highlightjs/angular-highlightjs.min.js"></script>


<script src="js/app.js"></script>
</body>
</html>
